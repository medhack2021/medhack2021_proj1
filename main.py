#%%
from objects.utils import combine_drug_info,scheduler

from objects.user import User
from objects.drug import Drug

all_drugs = combine_drug_info("./data/drug_info.csv","./data/drug_interaction.csv")
all_drugs.to_csv('./data/all_drugs.csv',index=False)


user1 = User('user0',0,(5,12,1948),sleep_hours=(21.0,6.0),motion_goal=[(6.0,7.0),(19.0,20.0)],breakfast_time=8, lunch_time=12, dinner_time=18,user_history={'prescription':['Glimepiride','Chlorthalidone','Simvastatin','Voriconazole','Tacrolimus']})
print(user1.motion_goal)

schedule, special_note = scheduler(user1,all_drugs)
print('schedule',schedule)
print('special_note',special_note)