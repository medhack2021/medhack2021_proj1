import numpy as np
import pandas as pd
from typing import List, Optional, Union, Tuple, Any, Dict, Type


class Drug(object):
   

    def __init__(self, drug_name:str, disease_name: str, drug_id:int,
                interval:float, dose: int = 1, Food: int = -1, alcohol: int = -1, tobacco: int = -1, 
                 rel_mean_time: List[float] = None, recommended_hours: List[Tuple[float]] = None, special_notes:str = None):
        """
        Constructor
        """
        self.drug_name = drug_name
        self.disease_name = disease_name
        self.dose = dose

        self.interval = interval
        self.Food = Food
        self.tobacco = tobacco
        self.alcohol = alcohol
        self.rel_mean_time = rel_mean_time
        self.recommended_hours = recommended_hours
        self.special_notes = special_notes



    # @classmethod
    # def get(drug, class_instance):
    #     """
    #     Helper function
    #     """
    #     new_obj = cls(class_instance.features_col, 
    #                   class_instance.target_col,
    #                   class_instance.pid_col,
    #                   class_instance.transforms)
    #     return new_obj


