"""

"""
from google.cloud import vision
import os

credential_path = "medhacks-proj-d548bfaa6545.json"
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path

def detect_text(path):
    """Detects text in the file."""
    
    import io
    import sys
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", sys.path)
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    print('Texts:')
    
    counter = 0
    for text in texts:
        #if counter == 0:
            #return text.description
        print('\n"{}"'.format(text.description))

        vertices = (['({},{})'.format(vertex.x, vertex.y)
                    for vertex in text.bounding_poly.vertices])

        print('bounds: {}'.format(','.join(vertices)))

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))
    
    

if __name__ == "__main__":
    print("!!!!!!!!!!!Before function call")
    global_path = "prescriptionSample2.png"
    detect_text(global_path)
    print("!!!!!!!!!!!After function call")
    # set GOOGLE_APPLICATION_CREDENTIALS="C:\Users\zhud4\OneDrive\Yale Concept\HackMed\medhacks-proj-d548bfaa6545.json"