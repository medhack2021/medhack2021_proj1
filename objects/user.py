import numpy as np
import pandas as pd
from typing import List, Optional, Union, Tuple, Any, Dict, Type


class User(object):
    
    def __init__(self, user_name:str, user_id:int, user_dob:Tuple[int], user_history:Dict = None,
        sleep_hours:Tuple[float] = None, motion_goal:List[Tuple[float]] = None, water_goal:List[float] = None, 
        breakfast_time:float=None, lunch_time:float=None, dinner_time:float=None):
        """
        Constructor
        user_history: {'prescription':list,'diagnose':list,('allergies':list,)}
        sleep_hours: ex. (21.0,6.0) meaning sleeping time is 9 pm - 6 am
        motion_goal: exx. [(7.0,8.0),(19.0,20.0)]
        """
        self.user_name = user_name
        self.user_id = user_id
        self.user_dob = user_dob
        self.user_history = user_history

        self.sleep_hours = sleep_hours
        self.motion_goal = motion_goal
        self.water_goal = water_goal
        self.breakfast_time = breakfast_time
        self.lunch_time = lunch_time
        self.dinner_time = dinner_time


