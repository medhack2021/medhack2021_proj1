#%%

import pandas as pd
import numpy as np
import os
import glob
from user import * #from .user import User

def combine_drug_info(info_csv_path,interaction_csv_path):
    info = pd.read_csv(info_csv_path)
    interaction = pd.read_csv(interaction_csv_path)
    #print(info)
    #print()
    #print(interaction)
    interaction_list = []
    for index, row in interaction.iterrows():
        row_dict = row.to_dict()
        #print(type(row))
        interaction_list.append(row_dict)
    info['interaction'] = interaction_list
    return info # dataframe

def isNaN(num):
    return num != num

def scheduler(usr,drug_info=None,time_interval=24.0):
    # initialize
    time_list = np.arange(0,time_interval,0.5)
    schedule = dict.fromkeys(time_list, " ")

    # sleep hours
    sleep_hour = usr.sleep_hours
    sleep_list = [x for x in time_list if x>=sleep_hour[0] or x<sleep_hour[1]]
    for x in sleep_list:
        schedule[x] = 'sleep'

    # meal hours
    meal_list = [usr.breakfast_time,usr.lunch_time,usr.dinner_time]
    print('meal list',meal_list)
    for x in meal_list:
        if schedule[x] == ' ':
            schedule[x] = 'meal'
        else:
            print('meal conflict with sleep, please check')
            break

    # motion hours
    motion_hour_list = usr.motion_goal
    motion_list = [x for x in time_list for y in motion_hour_list if x>=y[0] and x<y[1]]
    print('motion list',motion_list)
    for x in motion_list:
        if schedule[x] == ' ':
            schedule[x] = 'Exercise'
        else:
            print('exercise conflict with', schedule[x],', please check')
            break

    # fill in prescription
    prescription_list = usr.user_history['prescription']
    special_note_output = []
    for drug in prescription_list:
        # get basic info of the drug
        index = list(drug_info['Name']).index(drug)
        dose = drug_info['Dose'][index]
        interval = int(time_interval / drug_info['Interval'][index])
        food = drug_info['Food'][index]
        alcohol = drug_info['Alcohol'][index]
        tobacco = drug_info['Tobacco'][index]
        recom_hours = drug_info['Recommended_hours'][index]
        relat_hours = drug_info['Relative_time_for_meal'][index]
        special_note = drug_info['Special_note'][index]
        #print(drug,dose,interval,food,alcohol,tobacco,recom_hours,relat_hours,special_note,isNaN(special_note))

        # special notes
        if not isNaN(special_note):
            special_note_output.append(special_note)
        if alcohol == 0:
            special_note_output.append('Avoid_alcohol')
        if tobacco == 0:
            special_note_output.append('Avoid_tobacco')

        drug_hour_list = []
        # recommend hour first
        if not isNaN(recom_hours):
            recom_hours = tuple(recom_hours[1:-1].split(","))
            #print(recom_hours)
            drug_hour_list = [x for x in time_list if x>=int(recom_hours[0]) and x<int(recom_hours[1])]
            drug_hour_list = [x for x in drug_hour_list if schedule[x]==' ' or schedule[x]=='meal']
        else:
            drug_hour_list = [x for x in schedule.keys() if schedule[x]==' ' or schedule[x]=='meal']

        # consider meals
        if food == 1:
            drug_hour_list = [x for x in drug_hour_list if schedule[x]=='meal']

        #print(drug_hour_list)

        # relative time of meals
        if not isNaN(relat_hours):
            relat_hours_list = [x for x in schedule.keys() for y in meal_list if abs(x-y)<float(relat_hours)]
            print('relat_hours_list',relat_hours_list)
            drug_hour_list = [y for y in drug_hour_list if y not in relat_hours_list]
            #relat_hours_list = []
            #for x in list(relat_hours[1:-1].split(",")):
            #    relat_hours_list.extend([y+float(x) for y in meal_list])

            #print('relat_hours_list',relat_hours_list)
            #drug_hour_list = [y for y in relat_hours_list if y in drug_hour_list]


        #print(drug_hour_list)

        # interactions
        interactions = drug_info['interaction'][index]
        print(interactions, "!!!!!!!!", type(interactions)) #!!!!!!
        import ast
        interactions = ast.literal_eval(interactions)
        for other_drug in prescription_list:
            if other_drug==drug:
                continue
            severity = interactions[other_drug]
            if severity>0:
                times = [time for time,act in schedule.items() if other_drug in act]
                if len(times)>0:
                    conflict_time_list = [x for x in schedule.keys() for y in times if abs(x-y)<severity]
                    print('conflict_time_list',conflict_time_list)
                    drug_hour_list = [x for x in drug_hour_list if x not in conflict_time_list]

        #print(len(drug_hour_list),interval,drug_hour_list)
        if len(drug_hour_list)<interval:
            print('Strict prescription conflict, please check with the doctor')
            break
        else:
            # finalize the schedule
            drug_takes = [] #random.sample(drug_hour_list, interval)
            for i in range(interval):
                one_take = drug_hour_list[i]
                drug_takes.append(one_take)
                drug_hour_list = [x for x in drug_hour_list if abs(x-one_take)>1.5]

            for x in drug_takes:
                if schedule[x] == 'meal':
                    schedule[x] = schedule[x] + ', ' + drug + ', ' + str(dose) + ' pill(s)'
                else:
                    schedule[x] = drug + ', ' + str(dose) + ' pill(s)'
        #print(drug_hour_list)

    return schedule,set(special_note_output)

