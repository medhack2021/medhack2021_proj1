from flask import Flask, request, render_template
app = Flask(__name__)

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/test-page1/')
def test_page1():
  return render_template('test-page1.html')

@app.route('/my-link/')
def my_link():
  print ('I got clicked!')

  return 'Click.'

@app.route('/test-prog/')
def test_prog():
  print('This is an output to the terminal')

  return 'This is an output to the website ' + recursiveProgram()

@app.route('/sample_function/')
def sample_function():
  import random
  return "sample_function TEXT with randint " + str(random.randint(4, 8))

def recursiveProgram():
  y = text(32)
  output_text = y.output()
  a = 2
  string = "this is a string with a value of {}".format(a)
  return string + output_text

#@app.route('/getAddress', methods =["GET", "POST"])
#def getAddress():
  #if request.method == "POST" and "submit_address" in request.form:
    ## getting input with rx_address in HTML form
    #rx_address = request.form.get("rx_address")
    #print("here")
    #return "Your prescription address input is " + rx_address ##!!!!
  #return render_template("Extra2.html")

# A decorator used to tell the application
# which URL is associated function
@app.route('/gfgh', methods =["GET", "POST"])
def gfgh():
  import sys
  sys.path.append("../../objects")
  import user 
  import utils 
  #if user1 == None:
    #user1 = user.User("John Brown", 0, (0, 0, 0))
  if request.method == "POST" and "submit_info" in request.form:
    # getting input with name = fname in HTML form
    first_name = request.form.get("fname")
    # getting input with name = lname in HTML form 
    last_name = request.form.get("lname")
    # getting input with DOB = dob in HTML form
    date_of_birth = request.form.get("dob")
    fo = open("test.txt", "w")
    fo.write(first_name + " " + last_name + " " + date_of_birth + " ") #*******
    fo.close()
    """
    !!!!
    """
    print("there")
    return "Your name is "+ first_name + " " + last_name + ", and your date of birth is " + date_of_birth ##!!!!
  elif request.method == "POST" and "submit_address" in request.form:
    # getting input with rx_address in HTML form
    rx_address = request.form.get("rx_address")
    
    import ocrTest
    result = ocrTest.detect_text(rx_address)
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print(result)
    fo = open("test.txt", "a")
    fo.write("{'prescription':['Glimepiride','Chlorthalidone','Simvastatin'] }")
    fo.close()
    
    print("here")
    return "Your prescription address input is " + rx_address ##!!!!
  elif request.method == "POST" and "submit_bed_and_meal" in request.form:
    # getting input with bed and wake-up time in HTML form
    bed_time = request.form.get("sleep_bed")
    wake_time = request.form.get("sleep_wake")
    exe_start = request.form.get("exe_start")
    exe_end = request.form.get("exe_end")
    print("extra-here")
    breakfast_time = request.form.get("breakfast")
    lunch_time = request.form.get("lunch")
    dinner_time = request.form.get("dinner")
    
    fo = open("test.txt", "r")
    arg = fo.read()
    arguments = arg.split(" ")
    dob_parsed = tuple(arguments[2].split("/"))
    user1 = user.User(arguments[0] + " " + arguments[1], 0, dob_parsed)
    
    user1.user_history={'prescription':['Glimepiride','Chlorthalidone','Simvastatin']}
    
    user1.sleep_hours = (float(bed_time), float(wake_time))
    user1.motion_goal = [(float(exe_start), float(exe_end))]
    print(user1.motion_goal)
    user1.breakfast_time = float(breakfast_time)
    user1.lunch_time = float(lunch_time)
    user1.dinner_time = float(dinner_time)
    fo.close()
    
    import pandas as pd
    import sys
    import utils
    print(sys.path)
    #sys.path.append("../../data")
    #print(sys.path)
    all_drugs = pd.read_csv("../../data/all_drugs.csv")
    print("^^^^^^^^^^^^^^", type(all_drugs["interaction"][0]))
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", user1.user_name, "&&&&&&&&&&&&&&&&&&&&&&&", user1.user_history)
    schedule, special_note = utils.scheduler(user1,all_drugs)
    
    #return "Your approximate bedtime is " + bed_time + ", and your approximate time to get up is " + wake_time + ".\nYour approximate meal times are " + breakfast_time + ", " + lunch_time + ", " + dinner_time + " for breakfast, lunch and dinner, respectively\n\n\n\n\n" + str(schedule) + "\n\n\n\n\n" + str(special_note) 
    #return (schedule, special_note)
    #schedule["Your special note"] = str(special_note)
    return schedule
  print(request.form)
  return render_template("Extra1.html")



class text(object):
  def __init__(self, x):
    self.x = x

  def output(self):
    return "Test output" + str(self.x)


if __name__ == '__main__':
  user1 = None
  app.run(debug=True)
